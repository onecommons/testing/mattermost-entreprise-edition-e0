# Application Blueprint for Mattermost Enterprise Edition E0

## What is Mattermost?
Mattermost is an open source platform that provides secure collaboration for technical and operational teams that work in environments with complex nation-state level security and trust requirements.

**Documentation at:** <https://github.com/mattermost/mattermost-docker>

> Note: this version of Mattermost is the E0 version, and does not need a license to run. This version is recommended by Mattermost to allow one-click upgrade to E10 and E20 versions.

This Application Blueprint is made possible by the [Community Maintained One-Click Apps repository](https://github.com/caprover/one-click-apps).
If there are any apps you would like to help find their place in our Free and Open Cloud, consider contributing to One-Click Apps as well.
For more information on CapRover or their one One-Click Apps platform, visit [caprover.com](https://caprover.com/docs/one-click-apps.html).

`ensemble-generated.yaml` was generated from <https://github.com/caprover/one-click-apps/blob/master/public/v4/apps/mattermost-ee.yml>. Changes to `ensemble-template.yaml` will overwrite the generated values.
